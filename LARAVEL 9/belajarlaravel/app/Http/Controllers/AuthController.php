<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }

    public function welcome(Request $request)
    {
        $FirstName = $request->input('fname');
        $LastName = $request->input('lname');

        return view('page.home',['FirstName'=> $FirstName, 'LastName' => $LastName]);
    }
}
