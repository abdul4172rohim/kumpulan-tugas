<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas Pekan 5 Hari 2</title>
</head>
<body>
    <h1>Buat Account Baru<h1>
    <h4>Sign Up Form<h4>
    <form action="/welcome" method="post">
        @csrf
        <label for="">First Name</label> <br>
        <input type="text" name="fname"> <br> <br>
        <label for="">Last Name</label> <br>
        <input type="text" name="lname"> <br> <br>

        <label>Gender</label> <br>
        <input type="radio" name="le"> Male <br>
        <input type="radio" name="le"> Female <br>
        <input type="radio" name="le"> Other <br> <br> 

        <label>Nationality</label> <br> <br>
        <select name="nationality" id="">
            <option value ="1">Bahasa Indonesia</option>
            <option value ="2">English</option>
            <option value ="3">Other</option> <br> <br>
        </select> <br> <br>

        <label>Language Spoken</label> <br> <br>
        <input type="checkbox" name="language">Bahasa Indonesia <br>
        <input type="checkbox" name="language">English <br>
        <input type="checkbox" name="language">Other <br> <br>

        <label>Bio</label> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br> <br>

        <input type ="submit" value="Sign Up">
    </form>
</body>
</html>